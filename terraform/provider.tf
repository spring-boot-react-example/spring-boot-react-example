terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  # declaring where backend of terraform is
  backend "http" {}
}

# Configure the AWS Provider
provider "aws" {
  region = "${var.aws_region}"
  shared_credentials_file = "/Users/tf_user/.aws/credentials"
  profile                  = "default"
}